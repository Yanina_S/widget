(function() {
  if (document.body.querySelector(".widget")) {
    return;
  } else if (document.readyState === "complete") {
    createWidget();
  } else {
    document.onreadystatechange = function() {
      if (document.readyState === "complete") {
        createWidget();
      }
    };
  }

  let curentElement = null;

  function createWidget() {
    const widget = document.createElement("div");
    const wStyles = document.createElement("style");

    widget.innerHTML = `
      <div class="widget" id="widget">
        <div class="widget-top" id="widgetheader" style="cursor: move; margin-bottom: 15px; display: flex; justify-content: space-between; align-items: center;">
            <span>Search node element</span>
            <button id="w-close" class="w-close">&times;</button>
        </div>
        <div class="widget-search" style="margin-bottom: 15px;">
            <form class="widget-form" id="widget-form" style="display: flex;">
                <input type="search" required id="w-input-search" class="widget-form__search" name="q" style="flex: 1;" placeholder="Enter your selector">
                <button id="w-btn-search" class="widget-form__button">Search</button>
            </form>
        </div>
        <div class="widget-buttons" style="display: flex; justify-content: space-between; margin-bottom: 15px;">
            <button class="widget-buttons__button" type="button" id="w-btn-prev" disabled>Prev</button>
            <button class="widget-buttons__button" type="button" id="w-btn-next" disabled>Next</button>
            <button class="widget-buttons__button" type="button" id="w-btn-parent" disabled>Parent</button>
            <button class="widget-buttons__button" type="button" id="w-btn-children" disabled>Children</button>
        </div>
      </div>
    `;

    wStyles.innerHTML = `
      .widget {
        position: fixed;
        z-index: 9999;
        width: 330px;
        background: #ddd;
        right: 2px;
        top: 2px;
        box-sizing: border-box;
        padding: 17px 12px;
        box-shadow: 2px 2px 16px -2px rgba(0,0,0,0.5);
        border-radius: 2px;
        font-size: 16px;
        font-family: sans-serif;
      }
      .widget button {
        cursor: pointer;
        border: 1px solid #999;
      }
      .w-border {
        border: 1px solid red;
      }
      .widget button[disabled] {
        opacity: .3;
      }
      .widget-buttons__button {
        flex-basis: 24%;
      }
      .widget-form__button {
        margin-left: 4px;
      }
      .widget-form__search {
        border: 1px solid #999;
        background: #fbfbfc;
        padding: 3px 10px;
        font-size: 14px;
      }
    `;

    document.head.appendChild(wStyles);
    document.body.appendChild(widget);
    widget.addEventListener("click", mainCallback);
  }

  function closeWidget() {
    const widget = document.querySelector(".widget");
    widget.remove();
  }

  function getNode() {
    const wForm = document.getElementById("widget-form");
    wForm.addEventListener("submit", e => {
      e.preventDefault();
      enterValue = wForm.elements[0].value;
      findNodeDOM(enterValue);
      checkButtons(curentElement);
      wForm.reset();
    });
  }

  function findNodeDOM(el) {
    curentElement = document.querySelector(el);
    highLight(curentElement);
  }

  let highLighted;

  function highLight(el) {
    if (highLighted) {
      highLighted.classList.remove("w-border");
    }
    highLighted = el;
    highLighted.classList.add("w-border");
    highLighted.scrollIntoView();
  }

  function mainCallback(event) {
    let target = event.target;

    switch (target.id) {
      case "w-btn-search":
        getNode();
        break;
      case "w-close":
        closeWidget();
        break;
      case "w-btn-prev":
        prevHandler(curentElement);
        break;
      case "w-btn-next":
        nextHandler(curentElement);
        break;
      case "w-btn-parent":
        parentHandler(curentElement);
        break;
      case "w-btn-children":
        childHandler(curentElement);
        break;
    }
    checkButtons(curentElement);
  }

  function prevHandler(el) {
    curentElement = el.previousElementSibling;
    highLight(curentElement);
  }

  function nextHandler(el) {
    curentElement = el.nextElementSibling;
    highLight(curentElement);
  }

  function parentHandler(el) {
    curentElement = el.parentElement;
    highLight(curentElement);
  }

  function childHandler(el) {
    curentElement = el.firstElementChild;
    highLight(curentElement);
  }

  function checkButtons() {
    let btns = document.querySelectorAll(".widget-buttons__button");
    Array.from(btns).map(
      e => (e.disabled = !checkBtnWithElem(e, curentElement))
    );
  }

  function checkBtnWithElem(button, el) {
    if (button && el) {
      let isEnabled = true;
      switch (button.id) {
        case "w-btn-prev":
          isEnabled = Boolean(el.previousElementSibling);
          break;
        case "w-btn-next":
          isEnabled = Boolean(el.nextElementSibling);
          break;
        case "w-btn-parent":
          isEnabled = Boolean(el.parentElement);
          break;
        case "w-btn-children":
          isEnabled = Boolean(el.firstElementChild);
          break;
      }
      return isEnabled;
    }
  }

  dragElement(document.getElementById("widget"));

  function dragElement(elmnt) {
    var pos1 = 0,
      pos2 = 0,
      pos3 = 0,
      pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
      document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
      elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      elmnt.style.top = elmnt.offsetTop - pos2 + "px";
      elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
    }

    function closeDragElement() {
      document.onmouseup = null;
      document.onmousemove = null;
    }
  }
})();
