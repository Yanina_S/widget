$(function() {
  $(".aside-menu__link").click(function(e) {
    e.preventDefault();
    var $el = $(this).closest(".aside-menu__item");

    if ($($el).hasClass("aside-menu__item--arrow")) {
      $($el)
        .toggleClass("aside-menu__item--arrow-down")
        .children(".aside-menu__wrap")
        .toggleClass("aside-menu__wrap--open");
    }
  });
});
